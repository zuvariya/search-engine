from django.db import models
from django.contrib.auth.models import User


class Note(models.Model):
    user = models.CharField(max_length=10)
    pub_date = models.DateTimeField()
    title = models.CharField(max_length=200)
    body = models.TextField()
    #image=models.ImageField(upload_to='Note/')

    def __unicode__(self):
        return self.title
