import datetime
from haystack import indexes
from myapp.models import Note


class NoteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='user',faceted=True)
    pub_date = indexes.DateTimeField(model_attr='pub_date')
    suggestions = indexes.CharField() 
    #image=indexes.ImageField(model_attr='image')

    def get_model(self):
        return Note

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())

    def prepare(self, obj): 
        prepared_data = super(NoteIndex, self).prepare(obj) 
        prepared_data['suggestions'] = prepared_data['text'] 
        return prepared_data   

#class MySearchIndex(indexes.SearchIndex, indexes.Indexable):
 #   text = indexes.CharField(document=True, use_template=True)
    # ... normal fields then...
  #  suggestions = indexes.FacetCharField()

   # def prepare(self, obj):
    #    prepared_data = super(MySearchIndex, self).prepare(obj)
     #   prepared_data['suggestions'] = prepared_data['text']
      #  return prepared_data